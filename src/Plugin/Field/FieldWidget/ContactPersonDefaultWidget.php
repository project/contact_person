<?php

namespace Drupal\contact_person_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ContactPersonDefaultWidget' widget.
 *
 * @FieldWidget(
 *   id = "ContactPersonDefaultWidget",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "contact_person",
 *   },
 * )
 */
class ContactPersonDefaultWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta];
    $element['contact_person'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Contact Person'),
    ];
    $element['contact_person']['contact_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full Name'),
      '#default_value' => $item->contact_name,
      '#empty_value' => '',
    ];

    $element['contact_person']['contact_number'] = [
      '#type' => 'tel',
      '#title' => $this->t('Phone Number'),
      '#default_value' => $item->contact_number,
      '#empty_value' => '',
    ];

    $element['contact_person']['contact_mail'] = [
      '#type' => 'email',
      '#title' => $this->t('Mail'),
      '#default_value' => $item->contact_mail,
      '#empty_value' => '',
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      if (isset($values[$delta]['contact_person'])) {
        $values[$delta] = $values[$delta]['contact_person'];
      }
    }
    return $values;
  }

}
