<?php

namespace Drupal\contact_person_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;

/**
 * Plugin implementation of the 'contact_person' field type.
 *
 * @FieldType (
 *   id = "contact_person",
 *   label = @Translation("Contact Person"),
 *   description = @Translation("Stores number and mail."),
 *   category = @Translation("Custom"),
 *   default_widget = "ContactPersonDefaultWidget",
 *   default_formatter = "ContactPersonDefaultFormatter"
 * )
 */
class ContactPerson extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(StorageDefinition $field_definition) {
    return [
      'columns' => [
        'contact_name' => [
          'type' => 'varchar',
          'length' => 50,
          'not null' => FALSE,
        ],
        'contact_number' => [
          'type' => 'varchar',
          'length' => 32,
          'not null' => FALSE,
        ],
        'contact_mail' => [
          'type' => 'varchar',
          'length' => 32,
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value1 = $this->get('contact_name')->getValue();
    $value2 = $this->get('contact_number')->getValue();
    $value3 = $this->get('contact_mail')->getValue();
    return empty($value1) && empty($value2) && empty($value3);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(StorageDefinition $field_definition) {
    $properties = [];

    $properties['contact_name'] = DataDefinition::create('string')
      ->setLabel(t('Contact Person'))
      ->setDescription(t('The contact number'));

    $properties['contact_number'] = DataDefinition::create('string')
      ->setLabel(t('Contact Number'));

    $properties['contact_mail'] = DataDefinition::create('string')
      ->setLabel(t('Email'));

    return $properties;
  }


}
