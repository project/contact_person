<?php

namespace Drupal\contact_person_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal;

/**
 * Plugin implementation of the 'ContactPersonDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "ContactPersonDefaultFormatter",
 *   label = @Translation("Contact Person"),
 *   field_types = {
 *     "contact_person"
 *   }
 * )
 */
class ContactPersonDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   * Inside this method we can customize how the field is displayed inside
   * pages.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   * @param $langcode
   *
   * @return array
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->contact_name . ', ' . $item->contact_number . ', ' . $item->contact_mail,
      ];
    }

    return $elements;
  }

}
