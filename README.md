# Contact Person

Provides a new field type, which add ability to add Contact Person with name, phone and mail fields.

## Installation

Since the module requires an external library, Composer must be used.

### Composer

If your site is [managed via Composer](https://www.drupal.org/node/2718229), use Composer to
download the module, which will also download the required library:
   ```
   composer require "drupal/contact_person ~1.0"
   ```
